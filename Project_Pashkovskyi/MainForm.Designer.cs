﻿namespace Project_Pashkovskyi
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button_Report1 = new System.Windows.Forms.Button();
            this.button_AddApplication = new System.Windows.Forms.Button();
            this.button_Departments = new System.Windows.Forms.Button();
            this.button_AdvanceReport = new System.Windows.Forms.Button();
            this.button_ShowApplication = new System.Windows.Forms.Button();
            this.button_AddDepartment = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.button_AddWorker = new System.Windows.Forms.Button();
            this.Button_ShowWorkers = new System.Windows.Forms.Button();
            this.button_Report2 = new System.Windows.Forms.Button();
            this.button_Report3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.splitContainer2.Panel2.Controls.Add(this.button_Report3);
            this.splitContainer2.Panel2.Controls.Add(this.button_Report2);
            this.splitContainer2.Panel2.Controls.Add(this.button_Report1);
            this.splitContainer2.Panel2.Controls.Add(this.button_AddApplication);
            this.splitContainer2.Panel2.Controls.Add(this.button_Departments);
            this.splitContainer2.Panel2.Controls.Add(this.button_AdvanceReport);
            this.splitContainer2.Panel2.Controls.Add(this.button_ShowApplication);
            this.splitContainer2.Panel2.Controls.Add(this.button_AddDepartment);
            this.splitContainer2.Panel2.Controls.Add(this.button_Exit);
            this.splitContainer2.Panel2.Controls.Add(this.button_AddWorker);
            this.splitContainer2.Panel2.Controls.Add(this.Button_ShowWorkers);
            this.splitContainer2.Size = new System.Drawing.Size(1111, 525);
            this.splitContainer2.SplitterDistance = 877;
            this.splitContainer2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Result:";
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(24, 26);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(811, 464);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // button_Report1
            // 
            this.button_Report1.Location = new System.Drawing.Point(17, 255);
            this.button_Report1.Name = "button_Report1";
            this.button_Report1.Size = new System.Drawing.Size(75, 23);
            this.button_Report1.TabIndex = 7;
            this.button_Report1.Text = "Report 1";
            this.button_Report1.UseVisualStyleBackColor = true;
            this.button_Report1.Click += new System.EventHandler(this.button_Report1_Click);
            // 
            // button_AddApplication
            // 
            this.button_AddApplication.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_AddApplication.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.button_AddApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddApplication.Location = new System.Drawing.Point(116, 135);
            this.button_AddApplication.Name = "button_AddApplication";
            this.button_AddApplication.Size = new System.Drawing.Size(110, 24);
            this.button_AddApplication.TabIndex = 0;
            this.button_AddApplication.Text = "Add Application";
            this.button_AddApplication.UseVisualStyleBackColor = false;
            this.button_AddApplication.Click += new System.EventHandler(this.button_AddApplication_Click);
            // 
            // button_Departments
            // 
            this.button_Departments.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_Departments.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.button_Departments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Departments.Location = new System.Drawing.Point(116, 49);
            this.button_Departments.Name = "button_Departments";
            this.button_Departments.Size = new System.Drawing.Size(106, 37);
            this.button_Departments.TabIndex = 6;
            this.button_Departments.Text = "Show Departments";
            this.button_Departments.UseVisualStyleBackColor = false;
            this.button_Departments.Click += new System.EventHandler(this.button_Departments_Click);
            // 
            // button_AdvanceReport
            // 
            this.button_AdvanceReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_AdvanceReport.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.button_AdvanceReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AdvanceReport.Location = new System.Drawing.Point(116, 9);
            this.button_AdvanceReport.Name = "button_AdvanceReport";
            this.button_AdvanceReport.Size = new System.Drawing.Size(106, 34);
            this.button_AdvanceReport.TabIndex = 5;
            this.button_AdvanceReport.Text = "Show Advance \r\nReports";
            this.button_AdvanceReport.UseVisualStyleBackColor = false;
            this.button_AdvanceReport.Click += new System.EventHandler(this.button_AdvanceReport_Click);
            // 
            // button_ShowApplication
            // 
            this.button_ShowApplication.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_ShowApplication.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.button_ShowApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ShowApplication.Location = new System.Drawing.Point(3, 49);
            this.button_ShowApplication.Name = "button_ShowApplication";
            this.button_ShowApplication.Size = new System.Drawing.Size(107, 37);
            this.button_ShowApplication.TabIndex = 4;
            this.button_ShowApplication.Text = "Show Application";
            this.button_ShowApplication.UseVisualStyleBackColor = false;
            this.button_ShowApplication.Click += new System.EventHandler(this.button_ShowApplication_Click);
            // 
            // button_AddDepartment
            // 
            this.button_AddDepartment.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_AddDepartment.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.button_AddDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddDepartment.Location = new System.Drawing.Point(48, 165);
            this.button_AddDepartment.Name = "button_AddDepartment";
            this.button_AddDepartment.Size = new System.Drawing.Size(127, 25);
            this.button_AddDepartment.TabIndex = 3;
            this.button_AddDepartment.Text = "Add Department";
            this.button_AddDepartment.UseVisualStyleBackColor = false;
            this.button_AddDepartment.Click += new System.EventHandler(this.button_AddDepartment_Click);
            // 
            // button_Exit
            // 
            this.button_Exit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_Exit.Location = new System.Drawing.Point(60, 483);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(106, 30);
            this.button_Exit.TabIndex = 2;
            this.button_Exit.Text = "Exit";
            this.button_Exit.UseVisualStyleBackColor = false;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // button_AddWorker
            // 
            this.button_AddWorker.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_AddWorker.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.button_AddWorker.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddWorker.Location = new System.Drawing.Point(7, 135);
            this.button_AddWorker.Name = "button_AddWorker";
            this.button_AddWorker.Size = new System.Drawing.Size(103, 24);
            this.button_AddWorker.TabIndex = 1;
            this.button_AddWorker.Text = "Add Worker";
            this.button_AddWorker.UseVisualStyleBackColor = false;
            this.button_AddWorker.Click += new System.EventHandler(this.Button_AddWorker_Click);
            // 
            // Button_ShowWorkers
            // 
            this.Button_ShowWorkers.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Button_ShowWorkers.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Button_ShowWorkers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_ShowWorkers.Location = new System.Drawing.Point(3, 10);
            this.Button_ShowWorkers.Name = "Button_ShowWorkers";
            this.Button_ShowWorkers.Size = new System.Drawing.Size(107, 33);
            this.Button_ShowWorkers.TabIndex = 0;
            this.Button_ShowWorkers.Text = "ShowWorkers";
            this.Button_ShowWorkers.UseVisualStyleBackColor = false;
            this.Button_ShowWorkers.Click += new System.EventHandler(this.Button_ShowWorkers_Click);
            // 
            // button_Report2
            // 
            this.button_Report2.Location = new System.Drawing.Point(17, 285);
            this.button_Report2.Name = "button_Report2";
            this.button_Report2.Size = new System.Drawing.Size(75, 23);
            this.button_Report2.TabIndex = 8;
            this.button_Report2.Text = "Report 2";
            this.button_Report2.UseVisualStyleBackColor = true;
            this.button_Report2.Click += new System.EventHandler(this.button_Report2_Click);
            // 
            // button_Report3
            // 
            this.button_Report3.Location = new System.Drawing.Point(17, 315);
            this.button_Report3.Name = "button_Report3";
            this.button_Report3.Size = new System.Drawing.Size(75, 23);
            this.button_Report3.TabIndex = 9;
            this.button_Report3.Text = "Report 3";
            this.button_Report3.UseVisualStyleBackColor = true;
            this.button_Report3.Click += new System.EventHandler(this.button_Report3_Click);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(1111, 525);
            this.Controls.Add(this.splitContainer2);
            this.Name = "MainForm";
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button Button_ShowWorkers;
        private System.Windows.Forms.Button button_AddWorker;
        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Button button_AddDepartment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_ShowApplication;
        private System.Windows.Forms.Button button_AdvanceReport;
        private System.Windows.Forms.Button button_Departments;
        private System.Windows.Forms.Button button_AddApplication;
        private System.Windows.Forms.Button button_Report1;
        private System.Windows.Forms.Button button_Report2;
        private System.Windows.Forms.Button button_Report3;
    }
}


﻿namespace Project_Pashkovskyi
{
    partial class AddApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tripLocation = new System.Windows.Forms.TextBox();
            this.workerID = new System.Windows.Forms.TextBox();
            this.statusChangeDate = new System.Windows.Forms.TextBox();
            this.fillingTime = new System.Windows.Forms.TextBox();
            this.tripEndDate = new System.Windows.Forms.TextBox();
            this.tripStartDate = new System.Windows.Forms.TextBox();
            this.appStatus = new System.Windows.Forms.TextBox();
            this.tripReason = new System.Windows.Forms.TextBox();
            this.checkBox_ReportIDneed = new System.Windows.Forms.CheckBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.expensessCategory = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Currency = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox_Currency = new System.Windows.Forms.TextBox();
            this.sumOfMoney = new System.Windows.Forms.TextBox();
            this.advanceReportStatus = new System.Windows.Forms.TextBox();
            this.reportChangeStatusDate = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Worker ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Busines trip location";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Trip Started:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Trip Ended";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Time of filling application";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(314, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Advance report need?";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "App Status";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Status change date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(240, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 22);
            this.label11.TabIndex = 10;
            this.label11.Text = "Reason Of Trip?";
            // 
            // tripLocation
            // 
            this.tripLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tripLocation.Location = new System.Drawing.Point(166, 40);
            this.tripLocation.Name = "tripLocation";
            this.tripLocation.Size = new System.Drawing.Size(100, 21);
            this.tripLocation.TabIndex = 12;
            // 
            // workerID
            // 
            this.workerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workerID.Location = new System.Drawing.Point(166, 12);
            this.workerID.Name = "workerID";
            this.workerID.Size = new System.Drawing.Size(100, 21);
            this.workerID.TabIndex = 13;
            // 
            // statusChangeDate
            // 
            this.statusChangeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusChangeDate.Location = new System.Drawing.Point(166, 177);
            this.statusChangeDate.Name = "statusChangeDate";
            this.statusChangeDate.Size = new System.Drawing.Size(100, 21);
            this.statusChangeDate.TabIndex = 14;
            // 
            // fillingTime
            // 
            this.fillingTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillingTime.Location = new System.Drawing.Point(166, 122);
            this.fillingTime.Name = "fillingTime";
            this.fillingTime.Size = new System.Drawing.Size(100, 21);
            this.fillingTime.TabIndex = 15;
            // 
            // tripEndDate
            // 
            this.tripEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tripEndDate.Location = new System.Drawing.Point(166, 94);
            this.tripEndDate.Name = "tripEndDate";
            this.tripEndDate.Size = new System.Drawing.Size(100, 21);
            this.tripEndDate.TabIndex = 16;
            // 
            // tripStartDate
            // 
            this.tripStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tripStartDate.Location = new System.Drawing.Point(166, 68);
            this.tripStartDate.Name = "tripStartDate";
            this.tripStartDate.Size = new System.Drawing.Size(100, 21);
            this.tripStartDate.TabIndex = 17;
            // 
            // appStatus
            // 
            this.appStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appStatus.Location = new System.Drawing.Point(166, 151);
            this.appStatus.Name = "appStatus";
            this.appStatus.Size = new System.Drawing.Size(100, 21);
            this.appStatus.TabIndex = 20;
            // 
            // tripReason
            // 
            this.tripReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tripReason.Location = new System.Drawing.Point(25, 245);
            this.tripReason.Name = "tripReason";
            this.tripReason.Size = new System.Drawing.Size(576, 21);
            this.tripReason.TabIndex = 21;
            // 
            // checkBox_ReportIDneed
            // 
            this.checkBox_ReportIDneed.AutoSize = true;
            this.checkBox_ReportIDneed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_ReportIDneed.Location = new System.Drawing.Point(455, 16);
            this.checkBox_ReportIDneed.Name = "checkBox_ReportIDneed";
            this.checkBox_ReportIDneed.Size = new System.Drawing.Size(15, 14);
            this.checkBox_ReportIDneed.TabIndex = 24;
            this.checkBox_ReportIDneed.UseVisualStyleBackColor = true;
            this.checkBox_ReportIDneed.CheckedChanged += new System.EventHandler(this.checkBox_ReportIDneed_CheckedChanged);
            // 
            // button_Save
            // 
            this.button_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Save.Location = new System.Drawing.Point(112, 305);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(154, 38);
            this.button_Save.TabIndex = 25;
            this.button_Save.Text = "Save";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Cancel.Location = new System.Drawing.Point(331, 305);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(160, 38);
            this.button_Cancel.TabIndex = 26;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // expensessCategory
            // 
            this.expensessCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expensessCategory.Location = new System.Drawing.Point(452, 36);
            this.expensessCategory.Name = "expensessCategory";
            this.expensessCategory.Size = new System.Drawing.Size(100, 21);
            this.expensessCategory.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(314, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 15);
            this.label13.TabIndex = 28;
            this.label13.Text = "Expensess Category";
            // 
            // Currency
            // 
            this.Currency.AutoSize = true;
            this.Currency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Currency.Location = new System.Drawing.Point(314, 69);
            this.Currency.Name = "Currency";
            this.Currency.Size = new System.Drawing.Size(55, 15);
            this.Currency.TabIndex = 29;
            this.Currency.Text = "Currency";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(314, 94);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 15);
            this.label14.TabIndex = 30;
            this.label14.Text = "Sum Of Money";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(314, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(130, 15);
            this.label15.TabIndex = 31;
            this.label15.Text = "Advance Report Status";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(314, 151);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(156, 15);
            this.label16.TabIndex = 32;
            this.label16.Text = "Report Change Status Date";
            // 
            // textBox_Currency
            // 
            this.textBox_Currency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Currency.Location = new System.Drawing.Point(452, 65);
            this.textBox_Currency.Name = "textBox_Currency";
            this.textBox_Currency.Size = new System.Drawing.Size(100, 21);
            this.textBox_Currency.TabIndex = 33;
            // 
            // sumOfMoney
            // 
            this.sumOfMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sumOfMoney.Location = new System.Drawing.Point(452, 93);
            this.sumOfMoney.Name = "sumOfMoney";
            this.sumOfMoney.Size = new System.Drawing.Size(100, 21);
            this.sumOfMoney.TabIndex = 34;
            // 
            // advanceReportStatus
            // 
            this.advanceReportStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advanceReportStatus.Location = new System.Drawing.Point(452, 119);
            this.advanceReportStatus.Name = "advanceReportStatus";
            this.advanceReportStatus.Size = new System.Drawing.Size(100, 21);
            this.advanceReportStatus.TabIndex = 35;
            // 
            // reportChangeStatusDate
            // 
            this.reportChangeStatusDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportChangeStatusDate.Location = new System.Drawing.Point(452, 144);
            this.reportChangeStatusDate.Name = "reportChangeStatusDate";
            this.reportChangeStatusDate.Size = new System.Drawing.Size(100, 21);
            this.reportChangeStatusDate.TabIndex = 36;
            // 
            // AddApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(636, 355);
            this.Controls.Add(this.reportChangeStatusDate);
            this.Controls.Add(this.advanceReportStatus);
            this.Controls.Add(this.sumOfMoney);
            this.Controls.Add(this.textBox_Currency);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Currency);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.expensessCategory);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.checkBox_ReportIDneed);
            this.Controls.Add(this.tripReason);
            this.Controls.Add(this.appStatus);
            this.Controls.Add(this.tripStartDate);
            this.Controls.Add(this.tripEndDate);
            this.Controls.Add(this.fillingTime);
            this.Controls.Add(this.statusChangeDate);
            this.Controls.Add(this.workerID);
            this.Controls.Add(this.tripLocation);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddApplication";
            this.Text = "AddApplication";
            this.Load += new System.EventHandler(this.AddApplication_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tripLocation;
        private System.Windows.Forms.TextBox workerID;
        private System.Windows.Forms.TextBox statusChangeDate;
        private System.Windows.Forms.TextBox fillingTime;
        private System.Windows.Forms.TextBox tripEndDate;
        private System.Windows.Forms.TextBox tripStartDate;
        private System.Windows.Forms.TextBox appStatus;
        private System.Windows.Forms.TextBox tripReason;
        private System.Windows.Forms.CheckBox checkBox_ReportIDneed;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.TextBox expensessCategory;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label Currency;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox_Currency;
        private System.Windows.Forms.TextBox sumOfMoney;
        private System.Windows.Forms.TextBox advanceReportStatus;
        private System.Windows.Forms.TextBox reportChangeStatusDate;
    }
}
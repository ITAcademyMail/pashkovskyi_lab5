﻿namespace Project_Pashkovskyi
{
    partial class AddWorker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.first_name = new System.Windows.Forms.TextBox();
            this.dateOfBirth = new System.Windows.Forms.TextBox();
            this.adress = new System.Windows.Forms.TextBox();
            this.possition = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.mobileTelephone = new System.Windows.Forms.TextBox();
            this.patronymic = new System.Windows.Forms.TextBox();
            this.last_Name = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button_WorkerAdd = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.departmentId = new System.Windows.Forms.TextBox();
            this.button_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "E-mail:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mobile telephone:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Last name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Possition:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Adress:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Date of birth (YYYY-MM-DD):\r\n";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 18;
            // 
            // first_name
            // 
            this.first_name.Location = new System.Drawing.Point(164, 20);
            this.first_name.Name = "first_name";
            this.first_name.Size = new System.Drawing.Size(100, 20);
            this.first_name.TabIndex = 9;
            // 
            // dateOfBirth
            // 
            this.dateOfBirth.Location = new System.Drawing.Point(164, 126);
            this.dateOfBirth.Name = "dateOfBirth";
            this.dateOfBirth.Size = new System.Drawing.Size(100, 20);
            this.dateOfBirth.TabIndex = 10;
            // 
            // adress
            // 
            this.adress.Location = new System.Drawing.Point(164, 147);
            this.adress.Name = "adress";
            this.adress.Size = new System.Drawing.Size(100, 20);
            this.adress.TabIndex = 11;
            // 
            // possition
            // 
            this.possition.Location = new System.Drawing.Point(164, 169);
            this.possition.Name = "possition";
            this.possition.Size = new System.Drawing.Size(100, 20);
            this.possition.TabIndex = 12;
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(164, 106);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(100, 20);
            this.email.TabIndex = 14;
            // 
            // mobileTelephone
            // 
            this.mobileTelephone.Location = new System.Drawing.Point(164, 85);
            this.mobileTelephone.Name = "mobileTelephone";
            this.mobileTelephone.Size = new System.Drawing.Size(100, 20);
            this.mobileTelephone.TabIndex = 15;
            // 
            // patronymic
            // 
            this.patronymic.Location = new System.Drawing.Point(164, 64);
            this.patronymic.Name = "patronymic";
            this.patronymic.Size = new System.Drawing.Size(100, 20);
            this.patronymic.TabIndex = 16;
            // 
            // last_Name
            // 
            this.last_Name.Location = new System.Drawing.Point(164, 42);
            this.last_Name.Name = "last_Name";
            this.last_Name.Size = new System.Drawing.Size(100, 20);
            this.last_Name.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Patronymic";
            // 
            // button_WorkerAdd
            // 
            this.button_WorkerAdd.Location = new System.Drawing.Point(42, 231);
            this.button_WorkerAdd.Name = "button_WorkerAdd";
            this.button_WorkerAdd.Size = new System.Drawing.Size(200, 23);
            this.button_WorkerAdd.TabIndex = 21;
            this.button_WorkerAdd.Text = "Add Worker";
            this.button_WorkerAdd.UseVisualStyleBackColor = true;
            this.button_WorkerAdd.Click += new System.EventHandler(this.button_WorkerAdd_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 198);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Department id";
            // 
            // departmentId
            // 
            this.departmentId.Location = new System.Drawing.Point(164, 191);
            this.departmentId.Name = "departmentId";
            this.departmentId.Size = new System.Drawing.Size(100, 20);
            this.departmentId.TabIndex = 23;
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(105, 260);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(70, 23);
            this.button_Close.TabIndex = 24;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // AddWorker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 295);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.departmentId);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button_WorkerAdd);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.last_Name);
            this.Controls.Add(this.patronymic);
            this.Controls.Add(this.mobileTelephone);
            this.Controls.Add(this.email);
            this.Controls.Add(this.possition);
            this.Controls.Add(this.adress);
            this.Controls.Add(this.dateOfBirth);
            this.Controls.Add(this.first_name);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddWorker";
            this.Text = "Adding a worker";
            this.Load += new System.EventHandler(this.AddWorker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox first_name;
        private System.Windows.Forms.TextBox dateOfBirth;
        private System.Windows.Forms.TextBox adress;
        private System.Windows.Forms.TextBox possition;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox mobileTelephone;
        private System.Windows.Forms.TextBox patronymic;
        private System.Windows.Forms.TextBox last_Name;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button_WorkerAdd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox departmentId;
        private System.Windows.Forms.Button button_Close;
    }
}
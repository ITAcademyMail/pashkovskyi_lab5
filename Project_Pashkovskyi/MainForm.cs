﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Pashkovskyi
{
    public partial class MainForm : Form
    {
        private string connectionSettings =
            "Server=tcp:itacademyukraine.database.windows.net; Database=v.pashkovskyi; " +
            "User ID = v.pashkovskyi; Password=Dbiyz117; Trusted_Connection=False;Encrypt=True";

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Button_ShowWorkers_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("Select* From workers", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "workers");
                dataGridView2.DataSource = DS.Tables[0];

                sqlCon.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong... Please try again");

            }
        }
        private void button_ShowApplication_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("Select* From worker_application", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "worker_application");
                dataGridView2.DataSource = DS.Tables[0];

                sqlCon.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong... Please try again");

            }
        }

        private void button_AdvanceReport_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("Select* From worker_advance_report", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "worker_advance_report");
                dataGridView2.DataSource = DS.Tables[0];

                sqlCon.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong... Please try again");

            }
        }

        private void button_Departments_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("Select* From departments", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "departments");
                dataGridView2.DataSource = DS.Tables[0];

                sqlCon.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong... Please try again");

            }
        }

        

        private void Button_AddWorker_Click(object sender, EventArgs e)
        {
            AddWorker f = new AddWorker();
            f.Owner = this;
            f.Show();
        }

        private void button_AddDepartment_Click(object sender, EventArgs e)
        {
            AddDepartment d = new AddDepartment();
            d.Owner = this;
            d.Show();
        }

        private void button_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_AddApplication_Click(object sender, EventArgs e)
        {
            AddApplication ad = new AddApplication();
            ad.Owner = this;
            ad.Show();
        }

        private void button_Report1_Click(object sender, EventArgs e)
        {
            {
                //
                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("Select " +
                    " last_name, first_name, business_trip_location, reason, trip_start_date, trip_end_date,expensess_category, " +
                    "currency, sum_of_money, app_status, report_status FROM worker_application RIGHT JOIN workers  " +
                    "ON workers.worker_id = worker_application.worker_id RIGHT JOIN  worker_advance_report ON " +
                    "worker_application.application_id = worker_advance_report.report_id " +
                    "WHERE worker_application.business_trip_location = ('Лондон')" +
                    "AND worker_advance_report.currency = ('Фунти стерлінгів')" +
                    "AND worker_advance_report.report_status = ('Одобрено') ", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "worker_application");

                dataGridView2.DataSource = DS.Tables[0];
                sqlCon.Close();
                //
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button_Report2_Click(object sender, EventArgs e)
        {
            {
                
                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("SELECT last_name, first_name, patronymic " +
                    "FROM worker_application FULL JOIN workers  ON workers.worker_id = worker_application.worker_id  " +
                    "WHERE business_trip_location  IS NULL ", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "worker_application");

                dataGridView2.DataSource = DS.Tables[0];
                sqlCon.Close();
                
            }
        }

        private void button_Report3_Click(object sender, EventArgs e)
        {
            {

                SqlConnection sqlCon = new SqlConnection(connectionSettings);
                sqlCon.Open();

                SqlDataAdapter DA = new SqlDataAdapter("SELECT last_name, first_name, business_trip_location, reason, trip_start_date, " +
                    "trip_end_date, expensess_category, currency, sum_of_money, app_status " +
                    "FROM worker_application RIGHT JOIN workers  ON workers.worker_id = worker_application.worker_id " +
                    "LEFT JOIN  worker_advance_report ON worker_application.application_id = worker_advance_report.report_id ORDER BY sum_of_money DESC; ", sqlCon);
                SqlCommandBuilder CB = new SqlCommandBuilder(DA);
                DataSet DS = new DataSet();
                DA.Fill(DS, "worker_application");

                dataGridView2.DataSource = DS.Tables[0];
                sqlCon.Close();

            }
        }
    }

}


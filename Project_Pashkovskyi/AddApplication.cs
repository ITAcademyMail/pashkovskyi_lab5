﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Pashkovskyi
{
    public partial class AddApplication : Form
    {
        public AddApplication()
        {
            InitializeComponent();
             this.expensessCategory.Enabled = false;
                this.textBox_Currency.Enabled = false;
                this.sumOfMoney.Enabled = false;
                this.expensessCategory.Enabled = false;
                this.advanceReportStatus.Enabled = false;
                this.reportChangeStatusDate.Enabled = false;
        }
        private string connectionSettings =
            "Server=tcp:itacademyukraine.database.windows.net; Database=v.pashkovskyi; " +
            "User ID = v.pashkovskyi; Password=Dbiyz117; Trusted_Connection=False;Encrypt=True";

        private void checkBox_ReportIDneed_CheckedChanged(object sender, EventArgs e)
        {
            // настпуна конструкція передбачає, якщо до заяви на відрядження потрібно аванс, 
            //то зразу можна заповнити і авансовий звіт, але
            //не хоче по дефолту робити комірки не активними, тільки після кількох нажать
            if (checkBox_ReportIDneed.Checked.Equals(true))
            {
               
                this.expensessCategory.Enabled = true;
                this.textBox_Currency.Enabled = true;
                this.sumOfMoney.Enabled = true;
                this.expensessCategory.Enabled = true;
                this.advanceReportStatus.Enabled = true;
                this.reportChangeStatusDate.Enabled = true;
            }
            else
            {
                
                this.expensessCategory.Enabled = false;
                this.textBox_Currency.Enabled = false;
                this.sumOfMoney.Enabled = false;
                this.expensessCategory.Enabled = false;
                this.advanceReportStatus.Enabled = false;
                this.reportChangeStatusDate.Enabled = false;
            }
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            //не дозволяє ввести пусті рядки, чомусь не працює з NULL
            if (workerID.Text != "" && tripLocation.Text != "" && tripStartDate.Text != "" && tripEndDate.Text != "" && fillingTime.Text != "" && appStatus.Text != "" && tripReason.Text != "")
            {

                string query = "INSERT INTO worker_application VALUES ('" + workerID.Text + "','" + tripLocation.Text + "','" + tripReason.Text + "','" + tripStartDate.Text + "','" + tripEndDate.Text + "','" + fillingTime.Text + "','" + checkBox_ReportIDneed.Text + "','" + appStatus.Text + "','" + statusChangeDate.Text + "')";
                try
                {
                    using (SqlConnection sqlCon = new SqlConnection(connectionSettings))
                    {
                        sqlCon.Open();

                        SqlCommand sc = new SqlCommand(query, sqlCon);
                        sc.ExecuteNonQuery();

                        MessageBox.Show(query + " \n\n Query was succesfully complited!");

                        sqlCon.Close();

                        Close();

                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Something went wrong... Please try again");
                }

            }
            else
            {
                MessageBox.Show("Plesase fill all rows!");
            }

            if (checkBox_ReportIDneed.Checked.Equals(true))
            {
                if (workerID.Text != "" && expensessCategory.Text != "" && Currency.Text != "" && sumOfMoney.Text != "" && advanceReportStatus.Text != "" && reportChangeStatusDate.Text != "")
                {

                    string query = "INSERT INTO worker_advance_report VALUES ('" + workerID.Text + "','" + expensessCategory.Text + "','" + Currency.Text + "','" + sumOfMoney.Text + "','" + advanceReportStatus.Text + "','" + reportChangeStatusDate.Text + "')";
                    try
                    {
                        using (SqlConnection sqlCon = new SqlConnection(connectionSettings))
                        {
                            sqlCon.Open();

                            SqlCommand sc = new SqlCommand(query, sqlCon);
                            sc.ExecuteNonQuery();

                            MessageBox.Show(query + " \n\n Query was succesfully complited!");

                            sqlCon.Close();

                            Close();

                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Something went wrong... Please try again");
                    }

                }
                else
                {
                    MessageBox.Show("Plesase fill all rows!");
                }

            }
        }
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AddApplication_Load(object sender, EventArgs e)
        {

        }
    }
 }

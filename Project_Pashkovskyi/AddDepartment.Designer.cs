﻿namespace Project_Pashkovskyi
{
    partial class AddDepartment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.departmentID_TextBox = new System.Windows.Forms.TextBox();
            this.DepartmentName_TextBox = new System.Windows.Forms.TextBox();
            this.button_AddDepartment = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Department id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Department name:";
            // 
            // departmentID_TextBox
            // 
            this.departmentID_TextBox.Location = new System.Drawing.Point(127, 52);
            this.departmentID_TextBox.Name = "departmentID_TextBox";
            this.departmentID_TextBox.Size = new System.Drawing.Size(151, 20);
            this.departmentID_TextBox.TabIndex = 2;
            this.departmentID_TextBox.TextChanged += new System.EventHandler(this.departmentID_TextBox_TextChanged);
            // 
            // DepartmentName_TextBox
            // 
            this.DepartmentName_TextBox.Location = new System.Drawing.Point(127, 79);
            this.DepartmentName_TextBox.Name = "DepartmentName_TextBox";
            this.DepartmentName_TextBox.Size = new System.Drawing.Size(151, 20);
            this.DepartmentName_TextBox.TabIndex = 3;
            // 
            // button_AddDepartment
            // 
            this.button_AddDepartment.Location = new System.Drawing.Point(38, 119);
            this.button_AddDepartment.Name = "button_AddDepartment";
            this.button_AddDepartment.Size = new System.Drawing.Size(75, 23);
            this.button_AddDepartment.TabIndex = 4;
            this.button_AddDepartment.Text = "Add";
            this.button_AddDepartment.UseVisualStyleBackColor = true;
            this.button_AddDepartment.Click += new System.EventHandler(this.button_AddDepartment_Click);
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(175, 118);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 5;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // AddDepartment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 168);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.button_AddDepartment);
            this.Controls.Add(this.DepartmentName_TextBox);
            this.Controls.Add(this.departmentID_TextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddDepartment";
            this.Text = "Add Department";
            this.Load += new System.EventHandler(this.AddDepartment_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox departmentID_TextBox;
        private System.Windows.Forms.TextBox DepartmentName_TextBox;
        private System.Windows.Forms.Button button_AddDepartment;
        private System.Windows.Forms.Button button_Close;
    }
}